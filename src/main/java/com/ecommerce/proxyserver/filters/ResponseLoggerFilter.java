package com.ecommerce.proxyserver.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseLoggerFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(ResponseLoggerFilter.class);

    @Override
    public String filterType() {
        return FilterUtils.FILTER_TYPE_POST;
    }

    @Override
    public int filterOrder() {
        return FilterUtils.FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return FilterUtils.SHOULD_FILTER;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        logger.info("response status logged "+ctx.getResponse().getStatus());
        return null;
    }
}